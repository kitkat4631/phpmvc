<?php

namespace App\Services;

class Cart
{
	/**
	 * Retourne le tableau produits ajouter par l'utilisateur
	 * @return array Data Object
	 */
	public static function get(){
		return [];
	}

	/**
	 * Ajoute un produits dans le paniers
	 */
	public static function add($libelleProduit,$qteProduit,$prixProduit){
			if (creationPanier() && !isVerrouille())
			{
			   $positionProduit = array_search($libelleProduit,  $_SESSION['panier']['nomProduit']);
		 
			   if ($positionProduit !== false)
			   {
				  $_SESSION['panier']['qteProduit'][$positionProduit] += $qteProduit ;
			   }
			   else
			   {
				  array_push( $_SESSION['panier']['nomProduit'],$libelleProduit);
				  array_push( $_SESSION['panier']['qteProduit'],$qteProduit);
				  array_push( $_SESSION['panier']['prixProduit'],$prixProduit);
			   }
			}
			else
			echo "Un problème est survenu veuillez contacter l'administrateur du site.";
		 }
		}