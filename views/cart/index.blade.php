@extends('layouts')

@section('content')
<section class="container">
	<h1 class="title">Mon panier</h1>
	<hr>
	<table class="table is-striped is-narrow is-hoverable is-fullwidth">
		<thead>
			<tr>
				<th>Articles</th>
				<th>Prix</th>
				<th>Quantité</th>
			</tr>
			<?php
		if (creationPanier())
		{
			$nbArticles=count($_SESSION['panier']['nomProduit']);
			if ($nbArticles <= 0)
			echo "<tr><td>Votre panier est vide </ td></tr>";
			else
			{
				for ($i=0 ;$i < $nbArticles ; $i++)
				{
					echo "<tr>";
					echo "<td>".htmlspecialchars($_SESSION['panier']['nomProduit'][$i])."</ td>";
					echo "<td><input type=\"text\" size=\"4\" name=\"q[]\" value=\"".htmlspecialchars($_SESSION['panier']['qteProduit'][$i])."\"/></td>";
					echo "<td>".htmlspecialchars($_SESSION['panier']['prixProduit'][$i])."</td>";
					echo "<td><a href=\"".htmlspecialchars("panier.php?action=suppression&l=".rawurlencode($_SESSION['panier']['nomProduit'][$i]))."\">XX</a></td>";
					echo "</tr>";
				}
	
				echo "<tr><td colspan=\"2\"> </td>";
				echo "<td colspan=\"2\">";
				echo "Total : ".MontantGlobal();
				echo "</td></tr>";
	
				echo "<tr><td colspan=\"4\">";
				echo "<input type=\"submit\" value=\"Rafraichir\"/>";
				echo "<input type=\"hidden\" name=\"action\" value=\"refresh\"/>";
	
				echo "</td></tr>";
			}
		}
		?>
		</thead>
			<tbody>
					{{-- Blade : Boucles pour récupérer des produits commander ( https://laravel.com/docs/5.8/blade), 
							 Bulma : https://bulma.io/documentation/elements/table/--}}
			</tbody>
			<tfoot>
				<tr>
					<th>Articles</th>
					<th>Prix</th>
					<th>Quantité</th>
				<th>
					{{-- Afficher le prix totals de tout les produits --}}
					<h3 class="subtitle is-5">Total (€) : &euro;</h3>
						<?php function totalPrix(){
							if (isset($_SESSION['panier']))
							return count($_SESSION['panier']['prixProduit']);
							else
							return 0;
						} ?>
					{{-- afficher le nombre de produits aux totals --}}
					<h3 class="subtitle is-5">Nombre de produits : </h3>
					<?php function compterArticles(){
						if (isset($_SESSION['panier']))
						return count($_SESSION['panier']['nomProduit']);
						else
						return 0;
						} ?>
					<div class="buttons">
						<a href="/" class="button is-small is-default">Retour au shopping</a>
						<a href="/order" class="button is-small is-success">Valider la commande</a>
					</div>
				</th>
			</tr>
		</tfoot>
	</table>
</section>
@endsection